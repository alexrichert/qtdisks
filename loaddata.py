#!/usr/bin/env python
import dbus
from qtdisks_helperfunctions import *
###############################################################################
UDDrive = "org.freedesktop.UDisks2.Drive"
UDDAta = "org.freedesktop.UDisks2.Drive.Ata"
UDBlock = "org.freedesktop.UDisks2.Block"
UDFS = "org.freedesktop.UDisks2.Filesystem"
UDLoop = "org.freedesktop.UDisks2.Loop"
UDPart = "org.freedesktop.UDisks2.Partition"
UDPTable = "org.freedesktop.UDisks2.PartitionTable"
UDRaid = "org.freedesktop.UDisks2.MDRaid"
UDSwap = "org.freedesktop.UDisks2.Swapspace"
###############################################################################
class Device:
    pass

class Loop_wPT:
    pass

class MDRaid:
    pass

class Volume:
    pass

class Meta:
    pass
###############################################################################
def LoadData():
    systembus = dbus.SystemBus()
    udisks_obj = systembus.get_object('org.freedesktop.UDisks2', '/org/freedesktop/UDisks2')
    objectManager = dbus.Interface(udisks_obj, 'org.freedesktop.DBus.ObjectManager')
    rawData = [entry for entry in objectManager.GetManagedObjects().iteritems()]
    Devices = []
    Loops_wPT = []
    MDRaids = []

    addedEntry = 0 # check for double-counting

    for entry in rawData:
        if entry[1].has_key(UDDrive):
            Devices += [Device()]
            Devices[-1].drive = entry
            Devices[-1].volumes = []
            addedEntry += 1
        if entry[1].has_key(UDBlock) and entry[1].has_key(UDLoop) and entry[1].has_key(UDPTable):
            Loops_wPT += [Loop_wPT()]
            Loops_wPT[-1].loop = entry
            Loops_wPT[-1].volumes = []
            addedEntry += 1
        if entry[1].has_key(UDRaid):
            MDRaids += [MDRaid()]
            MDRaids[-1].mdraid = entry
            addedEntry += 1
        if addedEntry>1: print "No double dipping on drives!"
        addedEntry = 0

    nDevices = len(Devices)
    nLoops_wPT = len(Loops_wPT)

    for iDevice in xrange(nDevices):
        addedPTable = 0
        numberBlocks = 0
        for entry in rawData:
            if entry[1].has_key(UDBlock):
                if entry[1][UDBlock]["Drive"]==Devices[iDevice].drive[0]:
                    if entry[1].has_key(UDPTable):
                        Devices[iDevice].ptable = entry
                        addedPTable += 1
                    else:
                        Devices[iDevice].volumes += [Volume()]
                        Devices[iDevice].volumes[-1].block = entry
                        Devices[iDevice].volumes[-1].luks = []
                        Devices[iDevice].volumes[-1].raid = []
        if addedPTable>1: print "No double dipping on partition tables!"

    for iDevice in xrange(nDevices):
        for iVolume in xrange(len(Devices[iDevice].volumes)):
            for entry in rawData:
                if entry[1].has_key(UDBlock):
                    if entry[1][UDBlock]["CryptoBackingDevice"]==Devices[iDevice].volumes[iVolume].block[0]:
                        Devices[iDevice].volumes[iVolume].luks += [Meta()]
                        Devices[iDevice].volumes[iVolume].luks[-1].block = entry
                    if (Devices[iDevice].volumes[iVolume].block[1][UDBlock]["IdUsage"]=="raid") and (Devices[iDevice].volumes[iVolume].block[1][UDBlock]["MDRaidMember"]==entry[1][UDBlock]["MDRaid"]):
                        Devices[iDevice].volumes[iVolume].raid = entry
                if Devices[iDevice].volumes[iVolume].block[1][UDBlock]["MDRaidMember"]==entry[0]:
                    Devices[iDevice].volumes[iVolume].mdraid = entry

    DeviceNames = []
    for iDevice in xrange(len(Devices)):
        DeviceNames += [Devices[iDevice].drive[1][UDDrive]["SortKey"]]
        VolPaths = []
        for Vol in Devices[iDevice].volumes:
            VolPaths += [bytetostring(Vol.block[1][UDBlock]["Device"])]
        VolSort = [i[0] for i in sorted(enumerate(VolPaths), key=lambda x:x[1])]
        Devices[iDevice].volumes = [Devices[iDevice].volumes[i] for i in VolSort]
    DeviceSort = [i[0] for i in sorted(enumerate(DeviceNames), key=lambda x:x[1])]
    Devices = [Devices[i] for i in DeviceSort]
    return Devices, Loops_wPT, MDRaids
    systembus.close()
