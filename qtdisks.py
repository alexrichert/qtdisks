#!/usr/bin/env python
import sys
from PyQt4 import QtGui, QtCore, uic
import dbus
from dbus import Array as dbusArray
from dbus.mainloop.qt import DBusQtMainLoop
import subprocess
import pexpect
################################################################################
from qtdisks_helperfunctions import *
from loaddata import *
################################################################################
goToLastDisk = ""
goToLastVolume = ""
currentDevice = Device()
currentVolume = Volume()
currentDriveId = ""
currentVolSelection = ""

def _translate(context, text, disambig):
    return QtGui.QApplication.translate(context, text, disambig)
_fromUtf8 = QtCore.QString.fromUtf8

mountedicon = "dialog-ok-apply" # user-available
unmountedicon = "dialog-error" # user-busy

class Ui_MainWindow(QtGui.QMainWindow):
    def __init__(self):
        super(Ui_MainWindow, self).__init__()
        uic.loadUi('qtdisks.ui', self)
        self.show()
        # TRIGGERS:
        self.actionReload.triggered.connect(self.onReload)
        self.actionExit.triggered.connect(self.onExit)
        self.actionAbout.triggered.connect(self.onAbout)
        self.tree_disks.currentItemChanged.connect(self.onDriveClick)
        self.tree_volumes.currentItemChanged.connect(self.onVolumeClick)
        self.button_diskOnOff.clicked.connect(self.fbutton_diskOnOff)
        self.button_volOnOff.clicked.connect(self.fbutton_mountUnmountLockUnlock)
        self.connect(self.label_volStatus,QtCore.SIGNAL('clicked()'),self.openMountPoint)
        # DBus loop for auto-detecting device changes
        bus_loop = DBusQtMainLoop(set_as_default=True)
        bus = dbus.SystemBus(mainloop=bus_loop)
        iface  = 'org.freedesktop.DBus.ObjectManager'
        signaladd = 'InterfacesAdded'
        signalrm = 'InterfacesRemoved'
        bus.add_signal_receiver(self.onReload, signaladd, iface)
        bus.add_signal_receiver(self.onReload, signalrm, iface)
        # Reload on startup:
        self.onReload()

    def onReload(self,*args):
        global Devices
        global driveItems
        global raidNames
        global raidChildren
        global goToLastDisk
        global goToLastVolume
        goToLastDisk = currentDriveId
        goToLastVolume = currentVolSelection
        self.tree_disks.clear()
        self.clearDetailsVolumes()
        Devices, Loops_wPT, MDRaids = LoadData()
        nDevices = len(Devices)
        driveItems = []
        raidNames = []
        raidPaths = []
        raidIndices = []
        raidChildren = []
        for iDevice in xrange(nDevices):
            if len(Devices[iDevice].volumes)>0:
                if hasattr(Devices[iDevice].volumes[0],"mdraid"):
                    raidIndices += [iDevice]
        nRaidParents = len(MDRaids)
        nNonRaidDevices = 0
        for iDevice in xrange(nDevices):
            if not (iDevice in raidIndices):
                self.addDevice(iDevice)
                nNonRaidDevices += 1
        for iRaid in xrange(nRaidParents):
            raidname = MDRaids[iRaid].mdraid[1][UDRaid]["Name"]
            raidpath = MDRaids[iRaid].mdraid[0]
            self.addRAIDParent(raidname,raidpath,nNonRaidDevices,iRaid,raidIndices)
        self.tree_disks.expandAll()
        self.tree_disks.setRootIsDecorated(False)
        self.selectLastDisk()

    def onDriveClick(self):
        global currentDevice
        global currentDriveId
        self.clearDetailsVolumes()
        for device in Devices:
            if self.tree_disks.currentItem() is not None:
                if device.drive[1][UDDrive]["Id"]==self.tree_disks.currentItem().text(0):
                    currentDriveId = self.tree_disks.currentItem().text(0)
                    currentDevice = device
                    self.populateDiskDetails()
                    self.populateVolumeList()
                    self.selectLastVolume()
                    return 0
                if len(device.volumes)>0:
                    if hasattr(device.volumes[0],"mdraid"):
                        if device.volumes[0].mdraid[1][UDRaid]["Name"]==self.tree_disks.currentItem().text(0):
                            currentDriveId = self.tree_disks.currentItem().text(0)
                            currentDevice = device
                            self.populateRAIDDetails()
                            volpath = bytetostring(currentDevice.volumes[0].raid[1][UDBlock]["Device"])
                            QtGui.QTreeWidgetItem(self.tree_volumes)
                            self.tree_volumes.topLevelItem(0).setText(0, _translate("MainWindow", volpath, None))
                            self.tree_volumes.topLevelItem(0).setToolTip(0, _translate("MainWindow", volpath, None))
                            volstatus, statuscode, isnavable = self.getVolumeStatus(device.volumes[0])
                            if statuscode: self.tree_volumes.topLevelItem(0).setIcon(0,QtGui.QIcon.fromTheme(_fromUtf8(mountedicon)))
                            else: self.tree_volumes.topLevelItem(0).setIcon(0,QtGui.QIcon.fromTheme(_fromUtf8(unmountedicon)))
                            self.selectLastVolume()
                            return 0

    def populateVolumeList(self):
        if hasattr(currentDevice,"volumes") and currentDevice.drive[1][UDDrive]["MediaAvailable"]:
            volumeItems = []
            luksItems = []
            for iVolume in xrange(len(currentDevice.volumes)):
                volume = currentDevice.volumes[iVolume]
                if not hasattr(volume,"mdraid"):
                    volpath = bytetostring(volume.block[1][UDBlock]["Device"])
                    volumeItems += [QtGui.QTreeWidgetItem(self.tree_volumes)]
                    self.tree_volumes.topLevelItem(iVolume).setText(0, _translate("MainWindow", volpath, None))
                    self.tree_volumes.topLevelItem(iVolume).setToolTip(0, _translate("MainWindow", volpath, None))
                    volstatus, statuscode, isnavable = self.getVolumeStatus(volume)
                    if statuscode: 
                        if volume.block[1][UDBlock]["IdType"]=="crypto_LUKS":
                            self.tree_volumes.topLevelItem(iVolume).setIcon(0,QtGui.QIcon.fromTheme(_fromUtf8("changes-allow")))
                        else: self.tree_volumes.topLevelItem(iVolume).setIcon(0,QtGui.QIcon.fromTheme(_fromUtf8(mountedicon)))
                    elif volume.block[1][UDBlock]["IdType"]=="crypto_LUKS": self.tree_volumes.topLevelItem(iVolume).setIcon(0,QtGui.QIcon.fromTheme(_fromUtf8("changes-prevent")))
                    else: self.tree_volumes.topLevelItem(iVolume).setIcon(0,QtGui.QIcon.fromTheme(_fromUtf8(unmountedicon)))
                    for luks in volume.luks:
                        luksItems += [QtGui.QTreeWidgetItem()]
                        iLuks = len(luksItems)-1
                        self.tree_volumes.topLevelItem(iVolume).addChild(luksItems[iLuks])
                        lukspath = bytetostring(luks.block[1][UDBlock]["Device"])
                        luksItems[iLuks].setText(0, _translate("MainWindow", lukspath, None))
                        luksItems[iLuks].setToolTip(0, _translate("MainWindow", lukspath, None))
                        luksstatus, statuscode, isnavable = self.getVolumeStatus(luks)
                        if statuscode: luksItems[iLuks].setIcon(0,QtGui.QIcon.fromTheme(_fromUtf8(mountedicon)))
                        else: luksItems[iLuks].setIcon(0,QtGui.QIcon.fromTheme(_fromUtf8(unmountedicon)))
            self.tree_volumes.expandAll()
            self.tree_volumes.setRootIsDecorated(False)

    def populateRAIDDetails(self):
        raidvolume = currentDevice.volumes[0]
        raidname = raidvolume.mdraid[1][UDRaid]["Name"]
        raidtype = getRAIDType(raidvolume.mdraid[1][UDRaid]["Level"])
        raidsize = humansize(raidvolume.mdraid[1][UDRaid]["Size"])
        raidblockpath = bytetostring(raidvolume.raid[1][UDBlock]["Device"])
        rawstatus = raidvolume.mdraid[1][UDRaid]["Degraded"]
        if rawstatus:
            raidstatus = "Degraded"
        else:
            raidstatus = "OK"
        raidmembers = raidvolume.mdraid[1][UDRaid]["NumDevices"]
        self.setText(self.label_diskModel,"RAID array name: %s"%raidname)
        self.setText(self.label_diskSize,"Size: %s"%raidsize)
        self.setText(self.label_diskPartType,"RAID level: %s"%raidtype)
        self.setText(self.label_diskBlockPath,"Block device: %s"%raidblockpath)
        self.setText(self.label_diskSmart,"RAID status: %s (%s members)"%(raidstatus,raidmembers))
        self.populateDiskDetailsList(raidvolume.mdraid[1][UDRaid])

    def populateDiskDetails(self):
        deviceInfo = currentDevice.drive[1]
        smartstatus = "--"
        if deviceInfo.has_key(UDDAta):
            if deviceInfo[UDDAta]["SmartSupported"]:
                if deviceInfo[UDDAta]["SmartEnabled"]: smartstatus = deviceInfo[UDDAta]["SmartSelftestStatus"].capitalize()
                else: smartstatus = "Not enabled"
            else:
                smartstatus = "Not supported"
        devicemodel = deviceInfo[UDDrive]["Model"]
        devicesize = humansize(deviceInfo[UDDrive]["Size"])
        parttype = "--"
        if hasattr(currentDevice,"ptable"):
            parttype = currentDevice.ptable[1][UDPTable]["Type"]
            if parttype == "dos": parttype = "DOS (MBR)"
        elif len(currentDevice.volumes)>0:
            if hasattr(currentDevice.volumes[0],"mdraid"):
                parttype = "RAID member (%s)"%currentDevice.volumes[0].block[1][UDBlock]["IdLabel"]
        if hasattr(currentDevice,"ptable"): blockpath = bytetostring(currentDevice.ptable[1][UDBlock]["Device"])
        elif len(currentDevice.volumes)>0: blockpath = bytetostring(currentDevice.volumes[0].block[1][UDBlock]["Device"])
        else: blockpath = "--"
        self.setText(self.label_diskModel,"Model: %s"%devicemodel)
        self.setText(self.label_diskSize,"Size: %s"%devicesize)
        self.setText(self.label_diskPartType,"Partitioning: %s"%parttype)
        self.setText(self.label_diskBlockPath,"Block device: %s"%blockpath)
        self.setText(self.label_diskSmart,"SMART status: %s"%smartstatus)
        if deviceInfo[UDDrive]["Ejectable"] and deviceInfo[UDDrive]["ConnectionBus"]!="usb":
            if deviceInfo[UDDrive]["MediaAvailable"]:
                self.setText(self.label_diskSmart,"Drive status: %s inserted"%deviceInfo[UDDrive]["Media"].replace("optical_","").replace("_","-").upper())
                self.setText(self.button_diskOnOff,"Eject")
                self.button_diskOnOff.setIcon(QtGui.QIcon.fromTheme(_fromUtf8("media-eject")))
            else:
                self.setText(self.label_diskSmart,"Drive status: no detectable media")
        # Populate details list:
        self.populateDiskDetailsList(currentDevice.drive[1][UDDrive])
        if currentDevice.drive[1].has_key(UDDAta): self.populateDiskDetailsList(currentDevice.drive[1][UDDAta])

    def populateDiskDetailsList(self,sourcedata):
        keynames = sourcedata.keys()
        keynames.sort()
        for key in keynames:
            itemdata = sourcedata[key]
            if type(itemdata)==dbusArray:
                itemdata = str([str(chunk) for chunk in list(itemdata)])[1:-1].replace("'","")
            if itemdata == "0x5e83a97e51f14690": itemdata = ""
            if itemdata == "": itemdata = "--"
            itemtext = str(key)+": "+str(itemdata)
            item = QtGui.QListWidgetItem()
            self.list_diskDetails.addItem(item)
            self.setText(item,itemtext)

    def clearDetailsVolumes(self):
        self.list_diskDetails.clear()
        self.tree_volumes.clear()

    def clearVolumeDetails(self):
        self.setText(self.label_volName,"Name: --")
        self.setText(self.label_volSize,"Size: --")
        self.setText(self.label_volFS,"Filesystem: --")
        self.setText(self.label_volStatus,"Status: --")

    def addDevice(self,iDevice):
        global driveItems
        driveItems += [QtGui.QTreeWidgetItem(self.tree_disks)]
        iItemNumber = len(driveItems)-1
        deviceInfo = Devices[iDevice].drive[1][UDDrive]
        self.tree_disks.topLevelItem(iItemNumber).setText(0, _translate("MainWindow", deviceInfo["Id"], None))
        self.tree_disks.topLevelItem(iItemNumber).setToolTip(0, _translate("MainWindow", deviceInfo["Id"], None))
        if (deviceInfo["ConnectionBus"]=="usb" and deviceInfo["Ejectable"]) or deviceInfo["Media"][0:5]=="flash":
            self.tree_disks.topLevelItem(iItemNumber).setIcon(0,QtGui.QIcon.fromTheme(_fromUtf8("media-removable")))
        elif (deviceInfo["ConnectionBus"]=="usb" and not deviceInfo["Ejectable"]):
            self.tree_disks.topLevelItem(iItemNumber).setIcon(0,QtGui.QIcon.fromTheme(_fromUtf8("drive-harddisk-usb")))
        elif deviceInfo["Ejectable"]:
            self.tree_disks.topLevelItem(iItemNumber).setIcon(0,QtGui.QIcon.fromTheme(_fromUtf8("drive-optical")))
        else:
            self.tree_disks.topLevelItem(iItemNumber).setIcon(0,QtGui.QIcon.fromTheme(_fromUtf8("drive-harddisk")))

    def addRAIDParent(self,raidName,raidPath,nNonRaidDevices,iRaid,raidIndices):
        global driveItems
        global raidChildren
        driveItems += [QtGui.QTreeWidgetItem(self.tree_disks)]
        self.tree_disks.topLevelItem(nNonRaidDevices+iRaid).setText(0, _translate("MainWindow", raidName, None))
        self.tree_disks.topLevelItem(nNonRaidDevices+iRaid).setToolTip(0, _translate("MainWindow", raidName, None))
        self.tree_disks.topLevelItem(nNonRaidDevices+iRaid).setIcon(0,QtGui.QIcon.fromTheme(_fromUtf8("drive-multidisk")))
        for raidIndex in raidIndices:
            if Devices[raidIndex].volumes[0].block[1][UDBlock]["MDRaidMember"] == raidPath:
                raidChildren += [QtGui.QTreeWidgetItem()]
                self.tree_disks.topLevelItem(nNonRaidDevices+iRaid).addChild(raidChildren[-1])
                raidChildren[-1].setText(0, _translate("MainWindow", Devices[raidIndex].drive[1][UDDrive]["Id"], None))
                raidChildren[-1].setToolTip(0, _translate("MainWindow", Devices[raidIndex].drive[1][UDDrive]["Id"], None))
                raidChildren[-1].setIcon(0,QtGui.QIcon.fromTheme(_fromUtf8("drive-harddisk")))

    def onVolumeClick(self):
        global currentVolSelection
        global currentVolume
        self.clearVolumeDetails()
        if self.tree_volumes.currentItem() is not None:
            currentVolSelection = self.tree_volumes.currentItem().text(0)
            for volume in currentDevice.volumes:
                if bytetostring(volume.block[1][UDBlock]["Device"])==currentVolSelection:
                    currentVolume = volume
                    self.populateVolumeDetails()
                    return 0
                for luks in volume.luks:
                    if bytetostring(luks.block[1][UDBlock]["Device"])==currentVolSelection:
                        currentVolume = luks
                        self.populateVolumeDetails()
                        return 0
            if hasattr(currentDevice.volumes[0],"mdraid"):
                if bytetostring(currentDevice.volumes[0].raid[1][UDBlock]["Device"])==currentVolSelection:
                    currentVolume = volume
                    self.populateVolumeDetails()
                    return 0

    def fbutton_mountUnmountLockUnlock(self,MainWindow):
        if len(self.tree_volumes.selectedItems())==0:
            return 0
        global volstatus
        global luksstatus
        global lukspassphrase
        if hasattr(currentVolume,"mdraid"): currentVolPath = bytetostring(currentVolume.raid[1][UDBlock]["Device"]).rstrip('\0')
        else: currentVolPath = bytetostring(currentVolume.block[1][UDBlock]["Device"]).rstrip('\0')
        if currentVolPath!=self.tree_volumes.currentItem().text(0):
            return 0
        volstatus, statuscode, isnavable = self.getVolumeStatus(currentVolume)
        if volstatus == "Not mounted":
            p = subprocess.Popen(['udisksctl','mount','--block-device',currentVolPath], stdout=subprocess.PIPE, stderr=subprocess.PIPE)
            out, err = p.communicate()
        elif volstatus == "Locked":
            lukspassphrase, ok = QtGui.QInputDialog.getText(self, 'Unlock LUKS', 'Enter LUKS passphrase:',mode=QtGui.QLineEdit.Password)
            lukspassphrase = str(lukspassphrase)
            drive = str(currentVolPath)
            unlockproc = pexpect.spawn('udisksctl unlock -b ' + drive)
            unlockproc.expect('Passphrase:')
            unlockproc.sendline(lukspassphrase)
            lukspassphrase = ""
            try:
                unlockproc.expect(pexpect.EOF)
            except:
                pass
            unlockproc.close()
        elif volstatus == "Unlocked":
            lukscheck = 0
            for luksiter in currentVolume.luks:
                lukscheck += len(luksiter.block[1][UDFS]["MountPoints"])
            if lukscheck==0:
                p = subprocess.Popen(['udisksctl','lock','--block-device',currentVolPath], stdout=subprocess.PIPE, stderr=subprocess.PIPE)
                out, err = p.communicate()
        elif statuscode and currentVolume.block[1][UDBlock]["IdType"]!="swap": # make sure it's not swap; there's a better way to do this though
            p = subprocess.Popen(['udisksctl','unmount','--block-device',currentVolPath], stdout=subprocess.PIPE, stderr=subprocess.PIPE)
            out, err = p.communicate()
        self.onReload()

    def populateVolumeDetails(self):
        if hasattr(currentVolume,"mdraid"): block = currentVolume.raid
        else: block = currentVolume.block
        volumelabel = block[1][UDBlock]["IdLabel"]
        if len(volumelabel)==0: volumelabel = bytetostring(block[1][UDBlock]["Device"])
        volumesize = humansize(block[1][UDBlock]["Size"])
        volumefs = block[1][UDBlock]["IdType"]
        volumestatus, statuscode, isnavable = self.getVolumeStatus(currentVolume)
        self.setText(self.label_volName,"Name: %s"%volumelabel)
        self.setText(self.label_volSize,"Size: %s"%volumesize)
        self.setText(self.label_volFS,"Filesystem: %s"%volumefs)
        if isnavable:
            self.label_volStatus.setText(_translate("MainWindow", "Status: <u><FONT COLOR='#0000ff'>%s</u>"%volumestatus, None))
            ttname = volumestatus
            if len(ttname)>40: ttname = ttname[:15]+"..."+ttname[-15:]
            self.label_volStatus.setToolTip(_translate("MainWindow", "Click to open '%s' in default file manager"%ttname, None))
        else:
            self.setText(self.label_volStatus,"Status: %s"%volumestatus)
        self.updateVolButtons()

    def updateVolButtons(self):
        volstatus, statuscode, isnavable = self.getVolumeStatus(currentVolume)
        if currentVolume.block[1][UDBlock]["IdType"]=="swap":
            self.button_volOnOff.setEnabled(False)
        else: self.button_volOnOff.setEnabled(True)
        if volstatus == "Locked":
            self.setText(self.button_volOnOff,"Unlock LUKS device")
            self.button_volOnOff.setIcon(QtGui.QIcon.fromTheme(_fromUtf8("gcr-password")))
        elif volstatus == "Unlocked":
            self.setText(self.button_volOnOff,"Lock LUKS device")
            self.button_volOnOff.setIcon(QtGui.QIcon.fromTheme(_fromUtf8("changes-prevent")))
        elif volstatus == "Not mounted":
            self.setText(self.button_volOnOff,"Mount volume")
            self.button_volOnOff.setIcon(QtGui.QIcon.fromTheme(_fromUtf8("player_play")))
        elif statuscode:
            self.setText(self.button_volOnOff,"Unmount volume")
            self.button_volOnOff.setIcon(QtGui.QIcon.fromTheme(_fromUtf8("player_stop")))

    def getVolumeStatus(self,volume):
        if hasattr(volume,"mdraid"): checkvol = volume.raid
        else: 
            checkvol = volume.block
        if checkvol[1][UDBlock]["IdType"] == "swap":
            if checkvol[1][UDSwap]["Active"]: return "Active", True, False
            else: return "Inactive", False, False
        if checkvol[1][UDBlock]["IdType"]=="crypto_LUKS" and len(volume.luks)==0: return "Locked", False, False
        if checkvol[1][UDBlock]["IdType"]=="crypto_LUKS" and len(volume.luks)>0: return "Unlocked", True, False
        if not checkvol[1].has_key(UDFS):
            return "Not mounted", False, False
        if len(checkvol[1][UDFS]["MountPoints"])>0:
            return bytetostring(checkvol[1][UDFS]["MountPoints"][0]), True, True
        else:
            return "Not mounted", False, False

    def selectLastDisk(self):
        root = self.tree_disks.invisibleRootItem()
        parent_count = root.childCount()
        if parent_count > 0 and goToLastDisk=="":
            self.tree_disks.setCurrentItem(root.child(0))
            return 0
        for i in xrange(parent_count):
            parent = root.child(i)
            if str(parent.text(0)) == goToLastDisk:
                self.tree_disks.setCurrentItem(parent)
                return 0
            for irow in xrange(parent.childCount()):
                child = parent.child(irow)
                if str(child.text(0)) == goToLastDisk:
                    self.tree_disks.setCurrentItem(child)
                    return 0

    def selectLastVolume(self):
        root = self.tree_volumes.invisibleRootItem()
        parent_count = root.childCount()
        if parent_count > 0 and goToLastVolume=="":
            self.tree_volumes.setCurrentItem(root.child(0))
            return 0
        for i in xrange(parent_count):
            parent = root.child(i)
            if str(parent.text(0)) == goToLastVolume:
                self.tree_volumes.setCurrentItem(parent)
                return 0
            for irow in xrange(parent.childCount()):
                child = parent.child(irow)
                if str(child.text(0)) == goToLastVolume:
                    self.tree_volumes.setCurrentItem(child)
                    return 0
        self.tree_volumes.setCurrentItem(root.child(0))

    def setText(self,myobject,mytext):
        myobject.setText(_translate("MainWindow", mytext, None))
        myobject.setToolTip(_translate("MainWindow", mytext, None))

    def fbutton_diskOnOff(self):
        deviceInfo = currentDevice.drive[1]
        if deviceInfo[UDDrive]["Ejectable"] and deviceInfo[UDDrive]["ConnectionBus"]!="usb":
            if deviceInfo[UDDrive]["MediaAvailable"]:
                volumestatus, statuscode, isnavable = self.getVolumeStatus(currentVolume)
                if not statuscode:
                    p = subprocess.Popen(['eject',bytetostring(currentVolume.block[1][UDBlock]["Device"])], stdout=subprocess.PIPE, stderr=subprocess.PIPE)
                    out, err = p.communicate()

    def openMountPoint(self):
        volumestatus, statuscode, isnavable = self.getVolumeStatus(currentVolume)
        if isnavable:
            p = subprocess.Popen(['xdg-open',volumestatus], stdout=subprocess.PIPE, stderr=subprocess.PIPE)
            out, err = p.communicate()

    def onAbout(self):
        abouttext = "qtdisks\nby Alex Richert\nhttp://gitlab.com/alexrichert/qtdisks"
        aboutdialog = QtGui.QMessageBox.about(self, 'About qtdisks', abouttext)

    def onExit(self):
        sys.exit()


################################################################################

if __name__ == '__main__':
    app = QtGui.QApplication(sys.argv)
    window = Ui_MainWindow()
    app.exec_()
