from math import log
from PyQt4 import QtCore, QtGui

def bytetostring(arrbyte):
    return "".join(map(chr,arrbyte)).rstrip("\0")

def getRAIDType(raidlevel):
    if raidlevel=="raid0": return "0 (Striped)"
    if raidlevel=="raid1": return "1 (Mirrored)"
    return raidlevel.replace("raid","")

def humansize(size):
    size = int(size)
    if size>0:
        unit = ["B", "KB", "MB", "GB", "TB", "PB"]
        power = int(log(size,1000))
        scalar = str(round(float(size)/(1000.0**power),2))
        return scalar+str(unit[power])+" ("+str(size)+" bytes)"
    else:
        return "0 B"

def humanisize(size):
    size = int(size)
    if size>0:
        unit = ["B", "KiB", "MiB", "GiB", "TiB", "PiB"]
        power = int(log(size,1024))
        scalar = str(round(float(size)/(1024.0**power),2))
        return scalar+str(unit[power])+" ("+str(size)+" bytes)"
    else:
        return "0 B"

class ELabel(QtGui.QLabel):
    def paintEvent( self, event ):
        painter = QtGui.QPainter(self)
        metrics = QtGui.QFontMetrics(self.font())
        elided  = metrics.elidedText(self.text(), QtCore.Qt.ElideRight, self.width())
        painter.drawText(self.rect(), self.alignment(), elided)

class ClickableQLabel(QtGui.QLabel):
    def __init(self, parent):
        ELabel.__init__(self, parent)
    def mouseReleaseEvent(self, ev):
        self.emit(QtCore.SIGNAL('clicked()'))
