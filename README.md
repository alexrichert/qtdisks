qtdisks is (or, <em>will be</em>) a PyQt-based disk utility/UDisks2 front end, intended as a replacement for the GTK+-based GNOME Disks.

The main program is found in "qtdisks.py", and is therefore run using: <code>python qtdisks.py</code>

The other .py files have functions for obtaining UDisks data and miscellaneous convenience function, while qtdisks.ui is a QML file (created in qtcreator) containing the layout (which is loaded up directly in qtdisks.py).

Requirements include udisks2, PyQt4, dbus-python, python-pexpect, and xdg-utils. qtdisks has been developed under Fedora 21; package names may vary among distributions.

qtdisks is released under the <a href=http://opensource.org/licenses/BSD-3-Clause>BSD New (3-clause) license</a>. See http://www.qt.io/qt-licensing-terms/ for Qt licensing terms.
